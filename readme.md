# Lenguaje C

Desarrollo de aplicaciones en Lenguaje C. Este pretende ser un tutorial de
Lenguaje C desde lo más básico hasta lo más avanzado de la programación en este
lenguaje. 

## Paradigmas de programación en C

El lenguaje de programación C entra dentro de varias categorías:

1. Programación imperativa: Es el Paradigma de programación más antiguo, 
consiste en una en una secuencia claramene definida de instrucciones paso a paso
como si fuere una receta de cocina para un ordenador.
2. Programación funcional: En este Paradigma todo nuestro código estará dividido
en regiones de código llamado funciones o procedimiento. Una función es un trozo
de código que hace determinada cosa.

## Variables y tipos de datos

Los programas o aplicaciones trabajan, por lo general, con diferentes tipos de 
datos, y necesitan una manera para guardar los valores que están utilizando.
Estos valores pueden ser números o caracteres. El lenguaje C tiene dos maneras 
de guardar valores numéricos, variables y constantes. 

Una variable es una posición de almacenamiento de datos que tiene un valor que 
puede ser cambiado durante la ejecución del programa o aplicación.
Por el contrario, una constante mantiene un valor fijo que no puede cambiar 
durante la ejecución del programa.



1. int: 

